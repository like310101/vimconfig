set t_Co=256

"括号不自动匹配
let loaded_matchparen = 1

set dictionary=/usr/share/dict/words

"设置leader
let mapleader=","
"插件设置==============================================================================================================
"打开关闭目录树
nmap <silent> <F8> :NERDTreeToggle <cr>
"打开ctrlp插件查找文件
nmap <silent> <leader>ff <C-p>
"插件设置==============================================================================================================
"指定ctags搜寻tags的路径
set tag=tags;../tags;../../tags;../../../tags;../../../../../tags;../../../../../../tags;../../../../../../../../tags;
"突出显示当前行
"set cursorline
"覆盖文件时不备份
set nobackup
"指定一个tab键的缩进距离为4，默认为8
set tabstop=4 "when you click TAB,indent 4 character
"将tab键扩展为空格。例如这里你按一个tab键就相当于输入4个空格。这对python这种缩进对齐的语言很有用处
set expandtab "use <space> replace <Tab>
"将背景设置为dark，这会使你vi中的文字看上去比较亮。另一种是light模式，会使文字比较暗
set background=dark "this let comment line more light
"当你输入')','}',']'时，自动和前一个与之对应的'(','{','['匹配（用emacs的朋友肯定很熟悉这个功能）
set showmatch " let '(','{','[' can auto match by ')','}',']'
"设置匹配时间，太长了会让人觉得慢
set matchtime=2 " set matchtime = 10ms
"在normal模式时显示命令。命令显示在右下角
set showcmd
"显示行号
set nu "show line number at front
"智能搜索。在你搜索时，匹配你己经键入的字符。如果你认为已经找到需要的内容了，按enter键结束
set incsearch "when you serach, searching characters which you have entered
"当文本超过80个字符自动断行。它不会自动截断代码，但会截断注释。我就是用它来限制我每行注释不超过80个字符
set textwidth=80 "warp when more than 80 characters
"用set list命令可以查看文本中的tab键和空格键。但默认的符号太丑了，这个将tab键的显示换成 >---
set listchars=tab:>-,trail:- "when you enter 'set list', TAB will shown as '>---'
"在右下角显示当前光标所在的行号和列号
set ruler
"始终在屏幕下方显示状态栏。状态栏会显示文件名称、光标位置等等信息（有点类似emacs中那个）
set laststatus=2 "show status in bottom
"有些时候vim，特别是vim7安装后backspace键失灵。下面这个语句就是解决这个问题的
set bs=2 "this fix backspace cannot use in vim70
"高亮显示搜索结果
set hlsearch
" 将vim的光标设置为竖线
if has("autocmd")
   au InsertEnter * silent execute "!gconftool-2 --type string --set /apps/gnome-terminal/profiles/Default/cursor_shape ibeam"
   au InsertLeave * silent execute "!gconftool-2 --type string --set /apps/gnome-terminal/profiles/Default/cursor_shape block"
   au VimLeave * silent execute "!gconftool-2 --type string --set /apps/gnome-terminal/profiles/Default/cursor_shape block"
endif
"自动完成。在输入模式输入#b或者#e敲space键，就会用/*********或***********/替换。感觉没什么用
iab #b /*************************************************************************
iab #e <Space>**************************************************************************/
"打开智能缩进。这个是vi三种缩进中最聪明的一种。对写程序很方便
set cindent shiftwidth=4

filetype indent on

"用于cscope，当用cscope创建了tags后，在你光标所在的函数上
"按ctrl-] ctrl-[会跳转到该函数的调用处
map <C-]><C-[> :cs f 3 <cword><cr>

"下面6个映射用于在不退出输入模式的情况下移动光标
"具体方法就是同时按下ctrl和光标移动键(h or j or k or l)
"crtl-b，ctrl-w等同于normal模式下的b和w（前移一个单词和后移一个单词）
"注意，ctrl-h在输入模式下默认等同于backspace键（这和在shell下是一样的）
"另外，ctrl-e在输入模式默认是按照下一行的内容输入，很有用，所以注释掉了<c-e>的映射
" imap <C-h> <C-o>h

imap <C-j> <C-o>j

imap <C-k> <C-o>k

imap <C-l> <C-o>l

"imap <C-e> <C-o>e

imap <C-b> <C-o>b

imap <C-f> <C-o>w

"open preview window
"和ctags配合使用。用ctags创建了tags文件后，在你要查看定义的函数或变量上按
"shift-p shift-p 会在屏幕顶端打开一个占屏幕1/5的窗口，显示定义
"用shift-p shift-c可以关闭该窗口
"这个窗口在vi中称为预览窗口
map PP :ptag <C-R><C-W><cr><C-w><C-w>
nmap PC :pclose<cr>

"当一行很长，屏幕中一行显示不下时，vi会把自动把文本在屏幕断成几行。这时你无法用上下键移动到
"中间的行去（因为vi的光标移动依据逻辑行而不是屏幕行）。下面两个映射“重载”了小键盘的上下键，
"只要是屏幕显示出的行，你都可以用它们在其中上下移动
map <Up> gk
map <Down> gj

"打开高亮，等同syntax enable
"syntax on   

"关闭和vi兼容（很多功能都需要这个选项）
set nocp

"默认设置
if version>=600
        filetype plugin indent on
endif

"配色方案
colorscheme like310101
