#!/bin/sh

yum install -y ctags* ack
rm -f ~/.vimrc 2 > /dev/null
rm -fr ~/.vim 2 > /dev/null

cp -f vimrc ~/.vimrc
cp -fr vim ~/.vim
cp -f colors/* /usr/share/vim/vim72/colors/
